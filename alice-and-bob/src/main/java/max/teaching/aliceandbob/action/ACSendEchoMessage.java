package max.teaching.aliceandbob.action;

import max.core.action.Action;
import max.teaching.aliceandbob.agent.Alice;
import max.teaching.aliceandbob.role.RClient;

/**
 * An action to send an {@link max.teaching.aliceandbob.env.msg.MessageFactory.MessageTypes#ECHO}
 * message to all Bob agents.
 */
public class ACSendEchoMessage extends Action<Alice> {

  /** The message content. */
  private final String content;

  /**
   * Creates a new instance.
   *
   * @param environment where the action will run
   * @param owner who owns it
   * @param content the message's content
   */
  public ACSendEchoMessage(String environment, Alice owner, String content) {
    super(environment, RClient.class, owner);
    this.content = content;
  }

  @Override
  public void execute() {
    /* TO BE IMPLEMENTED */
  }

  @Override
  public <T extends Action<Alice>> T copy() {
    return (T) new ACSendEchoMessage(getEnvironment(), getOwner(), content);
  }
}
