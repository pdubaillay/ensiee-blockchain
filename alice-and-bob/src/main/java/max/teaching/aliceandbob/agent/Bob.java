package max.teaching.aliceandbob.agent;

import max.core.action.Plan;
import max.model.network.p2p.action.PeerAgent;
import max.teaching.aliceandbob.role.RServer;

/**
 * "Bob" agents are ECHO servers designed to respond to message received from "Alice" agents.
 *
 * The agent is configured to be able to play the {@link RServer} role.
 */
public class Bob extends PeerAgent {

  /**
   * Create a new {@link Bob} agents.
   * <p>
   * You need to provide a {@link Plan} that will be executed by the agents. The agents will make a copy
   * of it and set actions owner to itself. You can safely share plans with actions owner set to
   * {@code null}. All actions must support copy operation otherwise an exception is thrown.
   *
   * @param plan a non-null plan
   * @throws IllegalArgumentException {@code plan} is {@code null}
   */
  public Bob(Plan<? extends Bob> plan) {
    super(plan);
    addPlayableRole(RServer.class);
  }
}
