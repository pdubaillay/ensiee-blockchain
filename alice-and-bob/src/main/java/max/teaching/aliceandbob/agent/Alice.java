package max.teaching.aliceandbob.agent;

import max.core.action.Plan;
import max.model.network.p2p.action.PeerAgent;
import max.teaching.aliceandbob.role.RClient;

/**
 * "Alice" agents are clients designed to send messages to ECHO servers (called Bob).
 *
 * The agent is configured to be able to play the {@link RClient} role.
 */
public class Alice extends PeerAgent {

  /**
   * Create a new {@link Alice} agents.
   * <p>
   * You need to provide a {@link Plan} that will be executed by the agents. The agents will make a
   * copy of it and set actions owner to itself. You can safely share plans with actions owner set
   * to {@code null}. All actions must support copy operation otherwise an exception is thrown.
   *
   * @param plan a non-null plan
   * @throws IllegalArgumentException {@code plan} is {@code null}
   */
  public Alice(Plan<? extends Alice> plan) {
    super(plan);
    addPlayableRole(RClient.class);
  }
}
