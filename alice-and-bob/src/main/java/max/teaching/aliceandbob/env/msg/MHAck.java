package max.teaching.aliceandbob.env.msg;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.message.MessageHandler;

/**
 * A message handler that handles
 * {@link max.teaching.aliceandbob.env.msg.MessageFactory.MessageTypes#ACK} messages.
 */
public class MHAck implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    /* TO BE IMPLEMENTED */
  }
}
