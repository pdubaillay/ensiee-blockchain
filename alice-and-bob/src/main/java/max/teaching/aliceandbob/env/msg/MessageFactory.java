package max.teaching.aliceandbob.env.msg;

import java.util.List;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;

/**
 * A factory type to create configured messages.
 * <p>
 * Can't be instantiated.
 */
public final class MessageFactory {

  /**
   * The different message types the system can manage.
   */
  public enum MessageTypes {
    ACK,
    ECHO
  }

  /**
   * Creates a new {@link MessageTypes#ECHO} message given the recipient and the payload.
   *
   * @param sender    the address of the message's sender
   * @param recipients a list of recipients for the message
   * @param payload   the content of the message
   * @return a {@link Message} instance filled to be an {@link MessageTypes#ECHO}.
   */
  public static Message<AgentAddress, ?> createEchoMessage(AgentAddress sender,
      List<AgentAddress> recipients, String payload) {
    /* TO BE IMPLEMENTED */
    return null;
  }

  /**
   * Creates an {@link MessageTypes#ACK} message from the provided {@link MessageTypes#ECHO}
   * message.
   *
   * @param sender  address of the message's sender
   * @param echoMsg the message to reply
   * @return a {@link Message} instance filled to be an {@link MessageTypes#ACK}
   */
  public static Message<AgentAddress, ?> createAckMessage(AgentAddress sender,
      Message<AgentAddress, ?> echoMsg) {
    final var msg = new Message<>(
        sender,
        List.of(echoMsg.getSender()),
        echoMsg.getPayload()
    );
    msg.setType(MessageTypes.ACK.toString());
    return msg;
  }

  /**
   * Hide default implicit public constructor to prevent instantiation.
   */
  private MessageFactory() {
    // Do nothing
  }


}
