package max.teaching.aliceandbob.env;

import max.model.network.p2p.env.P2PEnvironment;
import max.teaching.aliceandbob.role.RClient;
import max.teaching.aliceandbob.role.RServer;

/**
 * A dedicated network-capable environment for our simulation.
 * <p>
 * {@link NetworkEnvironment} should allow {@link RClient} and {@link RServer} roles and provide a
 * dedicated context.
 */
public class NetworkEnvironment extends P2PEnvironment {

  public NetworkEnvironment() {
    addAllowedRole(RServer.class);
    addAllowedRole(RClient.class);
  }

  /* TO BE COMPLETED */
}
