package max.teaching.aliceandbob.exp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import max.core.EnvironmentEvent;
import max.core.MAXParameters;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.action.ReactiveAction;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.network.p2p.action.ACHandleMessages;
import max.model.network.p2p.env.message.MessageEvent;
import max.teaching.aliceandbob.action.ACSendEchoMessage;
import max.teaching.aliceandbob.agent.Alice;
import max.teaching.aliceandbob.agent.Bob;
import max.teaching.aliceandbob.env.NetworkEnvironment;
import max.teaching.aliceandbob.env.msg.MHAck;
import max.teaching.aliceandbob.env.msg.MHEcho;
import max.teaching.aliceandbob.env.msg.MessageFactory.MessageTypes;
import max.teaching.aliceandbob.role.RClient;
import max.teaching.aliceandbob.role.RServer;

/**
 * The scenario we are going to use to test our model.
 * <p>
 * Agents: n Alice and one Bob
 * <p>
 * Scenario:
 * <ol>
 *   <li>Alice send a message to Bob</li>
 *   <li>Bob prints the message's content on the console and reply to Alice with the same content</li>
 *   <li>Alice prints the message's content to the console.</li>
 * </ol>
 */
public class EchoScenario extends ExperimenterAgent {

  /**
   * How many Alice agents to start.
   */
  private final int aliceCount;

  /**
   * Creates a new instance of {@link EchoScenario}.
   * <p>
   * The count of Alice agents is set according to the value of the "ALICE_COUNT" parameter, default
   * is 1.
   */
  public EchoScenario() {
    aliceCount = MAXParameters.getIntegerParameter("ALICE_COUNT", 1);
  }


  @Override
  protected List<MAXAgent> setupScenario() {
    // All create agents
    final var agents = new ArrayList<MAXAgent>();

    // Create the environment
    final var env = new NetworkEnvironment();
    env.setName("Network");
    agents.add(env);

    // Create Alice(s)
    for (var i = 0; i < aliceCount; ++i) {
      final var alice = new Alice(createAlicePlan(env.getName()));
      alice.setName("Alice-" + (i + 1));
      agents.add(alice);
    }

    // Create Bob
    final var bob = new Bob(createBobPlan(env.getName()));
    bob.setName("Bob");
    agents.add(bob);

    return agents;
  }

  /**
   * Creates a new plan for an Alice agents.
   *
   * @param envName name of the environment where agents are evolving
   * @return a plan
   */
  private Plan<Alice> createAlicePlan(String envName) {
    return new Plan<>() {
      @Override
      public List<ActionActivator<Alice>> getInitialPlan() {
        /* TO BE IMPLEMENTED */
        return List.of();
      }

      @Override
      public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<Alice>>> getEventBoundActions() {
        /* TO BE IMPLEMENTED */
        return Map.of();
      }
    };
  }

  /**
   * Creates a new plan for a Bob agents.
   *
   * @param envName name of the environment where agents are evolving
   * @return a plan
   */
  private Plan<Bob> createBobPlan(String envName) {
    return new Plan<>() {
      @Override
      public List<ActionActivator<Bob>> getInitialPlan() {
        return List.of(
            new ACTakeRole<Bob>(envName, RServer.class, null).oneTime(1)
        );
      }

      @Override
      public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<Bob>>> getEventBoundActions() {
        final var handler = new ACHandleMessages<Bob>(envName, null);
        handler.registerMessageType(MessageTypes.ECHO.toString(), new MHEcho());

        return Map.of(MessageEvent.class, List.of(handler));
      }
    };
  }
}
