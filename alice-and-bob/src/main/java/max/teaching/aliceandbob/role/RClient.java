package max.teaching.aliceandbob.role;

import max.model.network.p2p.role.RNetworkPeer;

/**
 * Any agents playing the role {@link RClient} can play actions related to the Alice person.
 * <p>
 * Since most Alice actions are about networking, taking the {@link RClient} role should also grant
 * the usage of networking actions requiring the {@link max.model.network.p2p.role.RNetworkPeer}
 * role.
 */
public interface RClient extends RNetworkPeer {

}
