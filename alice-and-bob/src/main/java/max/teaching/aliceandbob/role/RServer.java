package max.teaching.aliceandbob.role;

import max.model.network.p2p.role.RNetworkPeer;

/**
 * Any agents playing the role {@link RServer} can play actions related to the Bob person.
 * <p>
 * Since most Bob actions are about networking, taking the {@link RServer} role also grants
 * the usage of networking actions requiring the {@link max.model.network.p2p.role.RNetworkPeer}
 * role.
 */
public interface RServer extends RNetworkPeer {

}
