package max.teaching.helloworld;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;
import max.core.action.Action;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.SimulatedAgent;
import max.core.role.RActor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * A set of tests, so you can ensure you followed the basic requirements for the role, the agents, the
 * action and the experimenter.
 */
class Control {

  /**
   * Asserts that RCanSayHi role is well-defined
   */
  @Test
  @DisplayName("RCanSayHi is well-defined")
  void testRole() {
    // Type exists and is an interface
    final var type = assertTypeIsDefined("max.teaching.helloworld.role.RCanSayHi");
    assertTrue(type.isInterface(), "RCanSayHi is not an interface");

    // It extends interface RActor
    final var interfaces = type.getInterfaces();
    assertEquals(1, interfaces.length, "RCanSayHi should only extend RActor");
    assertTrue(Arrays.stream(interfaces).allMatch(e -> e.equals(RActor.class)),
        "Your role doesn't extend RActor.");

    // No methods
    assertEquals(0, type.getMethods().length, "Your role should be empty.");
  }

  /**
   * Asserts that CustomAgent is well-defined.
   *
   * @throws ReflectiveOperationException can't execute a method or create an instance.
   */
  @Test
  @DisplayName("CustomAgent is well-defined")
  void testAgent()
      throws ReflectiveOperationException {
    // Type exists and is a class
    final var type = assertTypeIsDefined("max.teaching.helloworld.agents.CustomAgent");
    assertFalse(type.isInterface(), "CustomAgent is not a regular class");

    // It extends class SimulatedAgent
    final var interfaces = type.getInterfaces();
    final var superClass = type.getSuperclass();
    assertEquals(0, interfaces.length, "CustomAgent should only extend SimulatedAgent");
    assertEquals(SimulatedAgent.class, superClass, "Your agents doesn't extend SimulatedAgent.");

    // One constructor taking a plan
    final var constructors = type.getConstructors();
    assertEquals(1, constructors.length,
        "CustomAgent should only have one constructor taking a plan.");
    final var constructor = constructors[0];
    assertEquals(1, constructor.getParameterCount(),
        "CustomAgent's constructor should only take a plan.");
    assertEquals(Plan.class, constructor.getParameterTypes()[0],
        "CustomAgent's constructor only argument should be a Plan instance.");

    // Can play RCanSayHi
    final var agt = constructor.newInstance((Plan<SimulatedAgent>) List::of);
    final var mth = type.getMethod("canPlayRole", Class.class);
    final var canPlay = (boolean) mth.invoke(agt,
        assertTypeIsDefined("max.teaching.helloworld.role.RCanSayHi"));

    assertTrue(canPlay, "CustomAgent must be able to play RCanSayHi.");
  }

  /**
   * Asserts that ACSayHi is well-defined.
   */
  @Test
  @DisplayName("ACSayHi is well-defined")
  void testAction() {
    // Type exists and is a class
    final var type = assertTypeIsDefined("max.teaching.helloworld.action.ACSayHi");
    assertFalse(type.isInterface(), "ACSayHi is not a regular class");

    // It extends class SimulatedAgent
    final var interfaces = type.getInterfaces();
    final var superClass = type.getSuperclass();
    assertEquals(0, interfaces.length, "ACSayHi should only extend Action");
    assertEquals(Action.class, superClass, "Your action doesn't extend Action.");

    // One constructor taking a string and a custom agents
    final var constructors = type.getConstructors();
    assertEquals(1, constructors.length, "ACSayHi should only have one constructor.");
    final var constructor = constructors[0];
    assertEquals(2, constructor.getParameterCount(),
        "ACSayHi's constructor should take an environment name and a owner.");
    assertEquals(String.class, constructor.getParameterTypes()[0],
        "CustomAgent's constructor first argument should be a String instance.");
    assertEquals(assertTypeIsDefined("max.teaching.helloworld.agents.CustomAgent"),
        constructor.getParameterTypes()[1],
        "CustomAgent's constructor second argument should be a CustomAgent instance.");
  }

  /**
   * Asserts that MyExperiment is well-defined.
   */
  @Test
  @DisplayName("MyExperiment is well-defined")
  void testExperimenter() {
    // Type exists and is a class
    final var type = assertTypeIsDefined("max.teaching.helloworld.exp.MyExperiment");
    assertFalse(type.isInterface(), "MyExperiment is not a regular class");

    // It extends class SimulatedAgent
    final var interfaces = type.getInterfaces();
    final var superClass = type.getSuperclass();
    assertEquals(0, interfaces.length, "MyExperiment should only extend ExperimenterAgent");
    assertEquals(ExperimenterAgent.class, superClass,
        "Your experimenter doesn't extend ExperimenterAgent.");
  }

  /**
   * Try to load the given type and fails if it can't.
   *
   * @param name full name of the type
   * @return the loaded type
   */
  private Class<?> assertTypeIsDefined(String name) {
    try {
      return Class.forName(name);
    } catch (ClassNotFoundException e) {
      fail(String.format("Can't find type %s", name));
    }
    throw new AssertionError("Should not happen");
  }


}
