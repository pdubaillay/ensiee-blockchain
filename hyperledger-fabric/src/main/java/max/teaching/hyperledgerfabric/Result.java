package max.teaching.hyperledgerfabric;

/**
 * The kind of objects returned by a chaincode execution.
 * <p>
 * It contains two information:
 * <ul>
 *   <li>The read/write set, which tells which assets have been read, modified, deleted and created</li>
 *   <li>Eventually a return value, for example if the chaincode is about retrieving an asset.</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public class Result {

  /** The read/write set. */
  private final ResultSet resultSet;

  /** The optional returned value. */
  private final Object returnValue;

  /**
   * Creates a new {@link Result} instance with the provided information.
   *
   * @param resultSet the read/write set
   * @param returnValue the returned value, which can be {@code null}
   * @throws IllegalArgumentException resultSet is {@code null}
   */
  public Result(ResultSet resultSet, Object returnValue) {
    if (resultSet == null) {
      throw new IllegalArgumentException();
    }
    this.resultSet = resultSet;
    this.returnValue = returnValue;
  }

  /**
   * Gets the read/write set.
   *
   * @return never {@code null}
   */
  public ResultSet getResultSet() {
    return resultSet;
  }

  /**
   * Gets the optional returned value.
   *
   * @return can be {@code null}
   */
  public Object getReturnValue() {
    return returnValue;
  }

}
