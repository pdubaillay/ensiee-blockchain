package max.teaching.hyperledgerfabric.agents;

import max.core.action.Plan;
import max.model.ledger.blockchain.role.RBlockchainClient;
import max.model.network.p2p.action.PeerAgent;

/**
 * The agent that will play the role of a client device sending transactions to a known endorsing
 * peer.
 * <p>
 * Clients can only play the {@link RBlockchainClient} role.
 *
 * @author Pierre Dubaillay
 */
public class ClientAgent extends PeerAgent {

  /**
   * Creates a new {@link ClientAgent} instance that will use the provided plan.
   *
   * @param plan the plan the agent will follow
   */
  public ClientAgent(Plan<? extends PeerAgent> plan) {
    super(plan);
    addPlayableRole(RBlockchainClient.class);
  }
}
