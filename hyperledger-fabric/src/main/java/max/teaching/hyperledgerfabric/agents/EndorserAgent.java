package max.teaching.hyperledgerfabric.agents;

import max.core.action.Plan;
import max.datatype.ledger.Transaction;
import max.model.ledger.blockchain.action.BlockchainAgent;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.ledger.blockchain.role.RTransactionExecutor;

/**
 * The agent that will endorse (aka execute) and validate transactions.
 * <p>
 * Endorsers also act on the behalf of clients to propagate a transaction proposal (endorsing part)
 * and then submit the transaction to the ordering service.
 *
 * @author Pierre Dubaillay
 */
public class EndorserAgent extends BlockchainAgent<Transaction> {

  /**
   * Creates a new {@link EndorserAgent} instance that will use the provided plan.
   *
   * @param plan the plan to follow
   */
  public EndorserAgent(Plan<? extends BlockchainAgent<Transaction>> plan) {
    super(plan);
    addPlayableRole(RTransactionEndorser.class);
    addPlayableRole(RTransactionExecutor.class);
  }

}
