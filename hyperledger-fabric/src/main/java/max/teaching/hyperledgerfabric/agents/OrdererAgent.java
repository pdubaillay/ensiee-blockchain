package max.teaching.hyperledgerfabric.agents;

import max.core.action.Plan;
import max.model.ledger.blockchain.role.RBlockCreator;
import max.model.network.p2p.action.PeerAgent;

/**
 * The ordering service.
 * <p>
 * For now, the ordering service is one agent collecting transactions and periodically creating
 * blocks dispatched to endorsers. In the future, the ordering service could be more complex and run
 * a BFT consensus.
 * <p>
 * The {@link OrdererAgent} can play the {@link RBlockCreator} role.
 *
 * @author Pierre Dubaillay
 */
public class OrdererAgent extends PeerAgent {

  /**
   * Creates a new {@link OrdererAgent} instance that will use the provided plan.
   *
   * @param plan the plan to follow
   */
  public OrdererAgent(Plan<? extends OrdererAgent> plan) {
    super(plan);
    addPlayableRole(RBlockCreator.class);
  }


}
