package max.teaching.hyperledgerfabric.env.msg.handlers;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.teaching.hyperledgerfabric.Chaincode;
import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.env.msg.MessageFactory;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalPayload;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalResponsePayload;

/**
 * A dedicated handler that handles {@link MessageFactory#TX_PROPOSAL} messages.
 * <p>
 * The way the message is processed depends on the message's origin. If the message is from a client
 * (i.e. playing the {@link max.model.ledger.blockchain.role.RBlockchainClient} role) then the
 * endorser is the relay. In that case, it initializes a memory area to store responses and
 * propagate to all endorsers, including itself. If the message is from another endorser, then the
 * transaction proposal is executed and the result sent back in a
 * {@link MessageFactory#TX_PROPOSAL_RESPONSE} message.
 *
 * @author Pierre Dubaillay
 */
public class MHTxProposal implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    final var ctx = (EndorserContext) p2PContext;
    final var payload = (TxProposalPayload) message.getPayload();
    final var me = ctx.getOwner()
        .getAgentAddressIn(ctx.getEnvironmentName(), RTransactionEndorser.class);

    final var role = message.getSender().getRole();
    if (RTransactionEndorser.class.getName().equals(role)) {
      // I'm not the one who is going to centralize responses, juste send back my reply
      TxProposalResponsePayload pRPayload;
      try {
        final var chaincodeClass = Class.forName(payload.getChaincodeID());
        final var constructor = chaincodeClass.getConstructors()[0];  // We suppose one constructor per chaincode class.
        final var instance = (Chaincode) constructor.newInstance(payload.getArgs().toArray());
        final var result = instance.execute(ctx);

        pRPayload = new TxProposalResponsePayload(payload.getId(), result);

      } catch (Exception e) {
        pRPayload = new TxProposalResponsePayload(payload.getId(), e);
      }
      ((P2PEnvironment) ctx.getEnvironment()).sendMessage(
          MessageFactory.createTxProposalResponseMessage(
              me, message.getSender(), pRPayload
          ));
    } else {
      // The sender is a client, I have to aggregate replies. Propagate to friends.
      ctx.setClientForProposal(payload.getId(), message.getSender());
      propagateToEndorsers(ctx, payload, me);
    }
  }

  /**
   * Sends the given payload to all endorsers (i.e. playing the {@link RTransactionEndorser} role),
   * including myself.
   *
   * @param ctx the endorser's context
   * @param txProposal the proposal payload
   * @param me the endorser's address
   */
  private void propagateToEndorsers(P2PContext ctx, TxProposalPayload txProposal,
      AgentAddress me) {
    final var endorsers = ctx.getOwner()
        .getAgentsWithRole(ctx.getEnvironmentName(), RTransactionEndorser.class);
    endorsers.add(me);

    final var msg = MessageFactory.createTxProposalMessage(
        me,
        endorsers,
        txProposal
    );
    ((P2PEnvironment) ctx.getEnvironment()).sendMessage(msg);
  }

}
