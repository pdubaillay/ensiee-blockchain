package max.teaching.hyperledgerfabric.env.msg.handlers;

import java.math.BigDecimal;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.datatype.ledger.blockchain.Block;
import max.model.ledger.blockchain.role.RTransactionExecutor;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.teaching.hyperledgerfabric.TxPayload;
import max.teaching.hyperledgerfabric.exceptions.ValidationException;
import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.env.msg.MessageFactory;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload.Status;

/**
 * A handler that handles {@link MessageFactory#BLOCK} messages.
 * <p>
 * Upon message receive, the handler will retrieve all the transactions and start validating them in
 * the provided order. Read entries are checked against the declared asset version and the current
 * asset's version while write entries are executed and the database's state is updated
 * accordingly.
 *
 * @author Pierre Dubaillay
 */
public class MHBlock implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    final var ctx = (EndorserContext) p2PContext;
    final var owner = ctx.getOwner();
    final var block = (Block<Transaction, BigDecimal>) message.getPayload();
    final var me = owner.getAgentAddressIn(ctx.getEnvironmentName(), RTransactionExecutor.class);

    // Try to validate each transaction
    for (final var tx : block.getTransactions()) {
      Status status = Status.VALIDATED;
      try {
        validate(ctx, tx);
      } catch (Exception e) {
        status = Status.NOT_VALIDATED;
      }

      // Notify the client about the validation status of its transaction
      final var txPayload = (TxPayload) tx.getPayload().getValue();
      ((P2PEnvironment) ctx.getEnvironment()).sendMessage(MessageFactory.createNotificationMessage(
          me, txPayload.getClient(), new NotificationPayload(status, txPayload.getProposalId())
      ));
    }

    // Append the block no matter what
    ctx.getBlockTree().append(block);

  }

  /**
   * Validates the given transaction.
   * <p>
   * First, the read part is validated to make sure that each read asset didn't change between the
   * transaction execution and the validation time.
   * <p>
   * Then the write part is validated, updating the database state (only if the read part validation
   * is successful)
   *
   * @param ctx endorser's context
   * @param tx the transaction to validate
   * @throws ValidationException the read part is not valid
   */
  private void validate(EndorserContext ctx, Transaction tx)
      throws ValidationException {
    final TxPayload payload = (TxPayload) tx.getPayload().getValue();
    final var toApply = payload.getResponses().get(0).getResult();

    // Validate reads
    for (final var entry : toApply.getResultSet().getReadSet()) {
      final var asset = ctx.getAsset(entry.getKey());
      if (asset == null) {
        throw new ValidationException("Unknown asset id=" + entry.getKey());
      } else if (asset.getVersion() != entry.getVersion()) {
        throw new ValidationException(entry.getVersion(), asset.getVersion());
      }
    }

    // Apply writes
    for (final var entry : toApply.getResultSet().getWriteSet()) {
      if (entry.isDelete()) {
        ctx.deleteAsset(entry.getKey());
      } else {
        try {
          final var cpy = entry.getValue().copy();
          ctx.storeAsset(cpy);
        } catch (ReflectiveOperationException e) {
          throw new ValidationException(e);  // Should not happen
        }
      }
    }
  }
}
