package max.teaching.hyperledgerfabric.env;

import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.datatype.com.Address;
import max.datatype.com.UUIDAddress;
import max.datatype.ledger.Transaction;
import max.model.ledger.blockchain.env.BlockchainContext;

/**
 * A customized {@link BlockchainContext} for orderers.
 *
 * @author Pierre Dubaillay
 */
public class OrdererContext extends BlockchainContext<Transaction> {

  /**
   * Creates a new {@link OrdererContext} instance.
   *
   * @param owner the orderer
   * @param simulatedEnvironment the associated environment
   */
  public OrdererContext(SimulatedAgent owner,
      SimulatedEnvironment simulatedEnvironment) {
    super(owner, simulatedEnvironment);
    getWallet().addAddress("HF", new UUIDAddress());
  }

  @Override
  public Address getAddress() {
    return getWallet().getAddress("HF");
  }
}
