package max.teaching.hyperledgerfabric.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.ledger.blockchain.role.RBlockCreator;
import max.model.ledger.blockchain.role.RBlockchainClient;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.ledger.blockchain.role.RTransactionExecutor;
import max.model.network.p2p.env.P2PEnvironment;
import max.teaching.hyperledgerfabric.agents.ClientAgent;
import max.teaching.hyperledgerfabric.agents.EndorserAgent;
import max.teaching.hyperledgerfabric.agents.OrdererAgent;

/**
 * A customized {@link P2PEnvironment} which allows the following roles to be played:
 * <ul>
 *   <li>{@link RBlockchainClient}</li>
 *   <li>{@link RTransactionEndorser}</li>
 *   <li>{@link RTransactionExecutor}</li>
 *   <li>{@link RBlockCreator}</li>
 * </ul>
 * <p>
 * The environment also provides a customized context for each agent type.
 *
 * @author Pierre Dubaillay
 */
public class Channel extends P2PEnvironment {

  /**
   * Creates a new {@link Channel} instance and allows the listed roles.
   */
  public Channel() {
    addAllowedRole(RTransactionEndorser.class);
    addAllowedRole(RTransactionExecutor.class);
    addAllowedRole(RBlockCreator.class);
    addAllowedRole(RBlockchainClient.class);
  }

  @Override
  protected Context createContext(SimulatedAgent agent) {
    if (agent instanceof EndorserAgent) {
      return new EndorserContext(agent, this);
    } else if (agent instanceof OrdererAgent) {
      return new OrdererContext(agent, this);
    } else if (agent instanceof ClientAgent) {
      return new ClientContext(agent, this);
    }
    return super.createContext(agent);
  }
}
