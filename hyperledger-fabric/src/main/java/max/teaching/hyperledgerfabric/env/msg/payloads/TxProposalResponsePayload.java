package max.teaching.hyperledgerfabric.env.msg.payloads;

import max.teaching.hyperledgerfabric.Result;

/**
 * The content of a
 * {@link max.teaching.hyperledgerfabric.env.msg.MessageFactory#TX_PROPOSAL_RESPONSE} message.
 * <p>
 * This payload carries three information:
 * <ul>
 *   <li>The proposal ID</li>
 *   <li>The proposal's execution result, if any</li>
 *   <li>An error, if the execution failed</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public class TxProposalResponsePayload {

  /** The execution's error, if any. */
  private final Throwable error;

  /** The execution's result, if any. */
  private final Result result;

  /** The proposal ID. */
  private final int proposalId;

  /**
   * Creates a new {@link TxProposalResponsePayload} instance by providing a result.
   *
   * @param proposalId the proposal ID
   * @param result the execution result
   * @throws IllegalArgumentException proposalId is a negative number or result is {@code null}
   */
  public TxProposalResponsePayload(int proposalId, Result result) {
    if (result == null || proposalId < 0) {
      throw new IllegalArgumentException();
    }
    this.error = null;
    this.result = result;
    this.proposalId = proposalId;
  }

  /**
   * Creates a new {@link TxProposalResponsePayload} instance by providing an error.
   *
   * @param proposalId the proposal ID
   * @param error the execution error
   * @throws IllegalArgumentException proposalId is a negative number or error is {@code null}
   */
  public TxProposalResponsePayload(int proposalId, Throwable error) {
    if (error == null || proposalId < 0) {
      throw new IllegalArgumentException();
    }
    this.error = error;
    this.result = null;
    this.proposalId = proposalId;
  }

  /**
   * Gets the execution's error.
   *
   * @return a {@link Throwable} instance or {@code null} if the execution went well
   */
  public Throwable getError() {
    return error;
  }

  /**
   * Gets the execution's result.
   *
   * @return a {@link Result} instance or {@code null} if the execution went not well
   */
  public Result getResult() {
    return result;
  }

  /**
   * Gets the proposal's ID.
   *
   * @return a positive number (0 included)
   */
  public int getProposalId() {
    return proposalId;
  }
}
