package max.teaching.hyperledgerfabric.env;

import java.math.BigDecimal;
import max.datatype.com.Address;
import max.datatype.com.UUIDAddress;
import max.datatype.ledger.Payload;
import max.datatype.ledger.Transaction;
import max.model.ledger.blockchain.ITransactionFactory;

/**
 * A utility class to create transactions.
 *
 * @author Pierre Dubaillay
 */
public final class TxFactory implements ITransactionFactory<Transaction> {

  @Override
  public Transaction createTransaction(Address ignored, Address ignored1, double ignored2,
      Payload payload) {
    return new Transaction(
        new UUIDAddress(),
        new UUIDAddress(),
        0d,
        payload
    );

  }

  @Override
  public Transaction createTransaction(Address address, Address address1, double v, Payload payload,
      BigDecimal bigDecimal) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Transaction createCoinbaseTransaction(Address address, Address address1, double v,
      double v1) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Transaction createCoinbaseTransaction(Address address, Address address1, double v,
      double v1, BigDecimal bigDecimal) {
    throw new UnsupportedOperationException();
  }
}
