package max.teaching.hyperledgerfabric.env.msg.handlers;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.blockchain.role.RTransactionExecutor;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.message.MessageHandler;
import max.teaching.hyperledgerfabric.env.ClientContext;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload;

/**
 * A dedicated handler that handles
 * {@link max.teaching.hyperledgerfabric.env.msg.MessageFactory#NOTIFICATION} messages.
 * <p>
 * Notifications are about two things:
 * <ul>
 *   <li>Whether an issued proposal is endorsed or not;</li>
 *   <li>Whether an endorsed proposal is valid or not.</li>
 * </ul>
 * <p>
 * In the first case, this handler displays the status (ENDORSED or NOT_ENDORSED) and the associated
 * proposal execution result (or error), if any. In the second case, the client collects
 * notifications until nb(endorsers) notifications about the same proposal are received. Then the
 * status is displayed.
 * <p>
 * Endorsers are expected to return the same result (all return VALID or all return NOT_VALID).
 *
 * @author Pierre Dubaillay
 */
public class MHNotification implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    final var payload = (NotificationPayload) message.getPayload();
    final var ctx = (ClientContext) p2PContext;
    final var logger = ctx.getOwner().getLogger();
    final var displayMsg = "Transaction " + payload.getProposalId() + " is ";
    var result = payload.getResult();

    switch (payload.getStatus()) {
      case ENDORSED:
        logger.info(displayMsg + "endorsed.");
        if (result != null) {
          logger.info(result.toString());
        }
        break;

      case VALIDATED:
        handleValidated(ctx, payload);
        break;

      case NOT_ENDORSED:
        logger.warning(displayMsg + "NOT endorsed.");
        if (result != null) {
          logger.warning(result.toString());
        }
        break;

      case NOT_VALIDATED:
        handleNotValidated(ctx, payload);
        break;
    }
  }

  /**
   * Handles
   * {@link max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload.Status#VALIDATED}
   * notifications.
   * <p>
   * Notifications are collected until it gets one notification per endorser. Then the status is
   * displayed.
   *
   * @param ctx     endorser's context
   * @param payload notification payload
   */
  private void handleValidated(ClientContext ctx, NotificationPayload payload) {
    final var owner = ctx.getOwner();
    final var endorsers = owner.getAgentsWithRole(ctx.getEnvironmentName(),
        RTransactionExecutor.class);
    final var validationCount = ctx.countOneValidForProposal(payload.getProposalId());
    if (endorsers.size() == validationCount) {
      ctx.clearValidCountForProposal(payload.getProposalId());
      owner.getLogger().info("Transaction " + payload.getProposalId() + " is validated.");
    }
  }

  /**
   * Handles
   * {@link
   * max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload.Status#NOT_VALIDATED}
   * notifications.
   * <p>
   * Notifications are collected until it gets one notification per endorser. Then the status is
   * displayed.
   *
   * @param ctx     endorser's context
   * @param payload notification payload
   */
  private void handleNotValidated(ClientContext ctx, NotificationPayload payload) {
    final var owner = ctx.getOwner();
    final var endorsers = owner.getAgentsWithRole(ctx.getEnvironmentName(),
        RTransactionExecutor.class);
    final var validationCount = ctx.countOneInvalidForProposal(payload.getProposalId());
    if (endorsers.size() == validationCount) {
      ctx.clearInvalidCountForProposal(payload.getProposalId());
      owner.getLogger().warning("Transaction " + payload.getProposalId() + " is NOT validated.");
    }
  }
}
