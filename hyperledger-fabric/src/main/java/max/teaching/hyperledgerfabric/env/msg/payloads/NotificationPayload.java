package max.teaching.hyperledgerfabric.env.msg.payloads;

import java.util.Objects;

/**
 * The payload of a {@link max.teaching.hyperledgerfabric.env.msg.MessageFactory#NOTIFICATION}
 * message.
 * <p>
 * The payload contains three information:
 * <ul>
 *   <li>The status: ENDORSED, NOT_ENDORSED, VALIDATED, NOT_VALIDATED</li>
 *   <li>The proposal ID</li>
 *   <li>An associated result, if applicable.</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public class NotificationPayload {

  /**
   * What kind of notification it is.
   *
   * @author Pierre Dubaillay
   */
  public enum Status {
    ENDORSED,
    NOT_ENDORSED,
    VALIDATED,
    NOT_VALIDATED
  }

  /**
   * Notification's status.
   */
  private final Status status;

  /**
   * Notification's proposal ID.
   */
  private final int proposalId;

  /**
   * Notification's associated result.
   */
  private final Object result;

  /**
   * Creates a new {@link NotificationPayload} instance without an associated result.
   *
   * @param status     the kind of notification
   * @param proposalId regarding which proposal
   */
  public NotificationPayload(Status status, int proposalId) {
    this(status, proposalId, null);
  }

  /**
   * Creates a new {@link NotificationPayload} instance with an associated result.
   *
   * @param status     the kind of notification
   * @param proposalId regarding which proposal
   * @param result     the associated result
   * @throws IllegalArgumentException status is {@code null} or proposalId is a negative number
   */
  public NotificationPayload(Status status, int proposalId, Object result) {
    if (status == null || proposalId < 0) {
      throw new IllegalArgumentException();
    }
    this.status = status;
    this.proposalId = proposalId;
    this.result = result;
  }

  /**
   * Gets the proposal ID.
   *
   * @return a positive integer (0 included)
   */
  public int getProposalId() {
    return proposalId;
  }

  /**
   * Gets the status.
   *
   * @return never {@code null}
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Gets the associated result.
   *
   * @return the result or {@code null}
   */
  public Object getResult() {
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotificationPayload that = (NotificationPayload) o;
    return proposalId == that.proposalId && status == that.status && Objects.equals(result,
        that.result);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, proposalId, result);
  }
}
