package max.teaching.hyperledgerfabric.env.msg.payloads;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The content of a {@link max.teaching.hyperledgerfabric.env.msg.MessageFactory#TX_PROPOSAL}
 * message.
 * <p>
 * Each proposal gets a unique positive ID and contains two more information:
 * <ul>
 *   <li>the chaincode ID to be executed (the qualified name of the chaincode's class)</li>
 *   <li>a list of arguments required by the chaincode invocation</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public class TxProposalPayload {

  /**
   * A thread-safe increasing counter to generate ids.
   */
  private static final AtomicInteger COUNTER = new AtomicInteger();

  /**
   * The chaincode's ID (class name).
   */
  private final String chaincodeID;

  /**
   * A list of arguments (possibly empty).
   */
  private final List<Object> args;

  /**
   * The proposal's ID.
   */
  private final int id;

  /**
   * Creates a new {@link TxProposalPayload} instance.
   *
   * @param chaincodeID chaincode's ID (class name)
   * @param args        a list of arguments
   * @throws IllegalArgumentException chaincode or args is {@code null}
   */
  public TxProposalPayload(String chaincodeID, List<Object> args) {
    if (chaincodeID == null || args == null) {
      throw new IllegalArgumentException();
    }
    this.chaincodeID = chaincodeID;
    this.args = args;
    id = COUNTER.getAndIncrement();
  }

  /**
   * Gets the chaincode's ID.
   *
   * @return never {@code null}
   */
  public String getChaincodeID() {
    return chaincodeID;
  }

  /**
   * Gets the list of arguments.
   *
   * @return never {@code null} but can be empty
   */
  public List<Object> getArgs() {
    return args;
  }

  /**
   * Gets the proposal's ID.
   *
   * @return a positive integer (0 included)
   */
  public int getId() {
    return id;
  }
}
