package max.teaching.hyperledgerfabric.env;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import madkit.kernel.AgentAddress;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.teaching.hyperledgerfabric.Asset;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalResponsePayload;

/**
 * A customized {@link max.model.ledger.blockchain.env.BlockchainContext} for endorsers.
 * <p>
 * This context stores:
 * <ul>
 *   <li>The world state (i.e. a mapping ID to Asset)</li>
 *   <li>The received proposal responses when the endorser is acting on the behalf of a client</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public class EndorserContext extends OrdererContext {

  /**
   * The world state, being a mapping ID to Asset.
   */
  private final Map<String, Asset> worldState;

  /**
   * All the received responses for a given proposal ID.
   */
  private final Map<Integer, List<TxProposalResponsePayload>> proposalResponses;

  /**
   * An association proposal ID to client address.
   */
  private final Map<Integer, AgentAddress> clientsPerProposal;

  /**
   * Creates a new {@link EndorserContext} instance.
   *
   * @param owner                the endorser
   * @param simulatedEnvironment the associated environment
   */
  public EndorserContext(SimulatedAgent owner, SimulatedEnvironment simulatedEnvironment) {
    super(owner, simulatedEnvironment);

    worldState = new HashMap<>();
    proposalResponses = new HashMap<>();
    clientsPerProposal = new HashMap<>();
  }

  /**
   * Gets a stored asset given its ID.
   *
   * @param id  the asset's ID
   * @param <A> the type of the asset to retrieve
   * @return the asset instance or {@code null} if there is no such instance
   * @throws ClassCastException the asset can't be cast to the provided type
   */
  public <A extends Asset> A getAsset(String id) {
    return (A) worldState.get(id);
  }

  /**
   * Stores the given asset in the world state.
   * <p>
   * Since two assets are considered identical if they have the same ID, the provided asset's
   * version will be incremented if a previous asset with the same ID already exists in the world
   * state.
   *
   * @param asset the asset to store
   */
  public void storeAsset(Asset asset) {
    final var id = asset.getId();
    final var oldAsset = getAsset(id);
    worldState.put(id, asset);

    if (oldAsset != null) {
      asset.increaseVersion();
    }
  }

  /**
   * Removes the asset with the provided asset ID.
   *
   * @param id id of the asset to delete
   */
  public void deleteAsset(String id) {
    worldState.remove(id);
  }

  /**
   * Stores the given proposal responses with others regarding the same proposal.
   *
   * @param proposalResponse the response to store
   * @throws IllegalArgumentException this agent doesn't act on the behalf of the proposal issuer
   */
  public void addProposalResponse(TxProposalResponsePayload proposalResponse) {
    final var responses = proposalResponses.get(proposalResponse.getProposalId());
    if (responses == null) {
      throw new IllegalArgumentException();
    }
    responses.add(proposalResponse);
  }

  /**
   * Gets the list of stored proposal responses for the given proposal ID.
   *
   * @param proposalId the ID
   * @return Either a possibly empty list or {@code null} if the agent doesn't store responses for
   * the provided proposal ID.
   */
  public List<TxProposalResponsePayload> getProposalResponsesFor(Integer proposalId) {
    return proposalResponses.getOrDefault(proposalId, List.of());
  }

  /**
   * Removes all information (proposal responses and client address) for the given proposal ID.
   * <p>
   * Does nothing if the endorser was not storing data for the given proposal ID.
   *
   * @param proposalId the proposal ID
   */
  public void forgotProposal(Integer proposalId) {
    proposalResponses.remove(proposalId);
    clientsPerProposal.remove(proposalId);
  }

  /**
   * Initializes a storage for storing both the client address and proposal responses related to the
   * given proposal ID.
   *
   * @param id the proposal ID
   * @param client the client's address
   */
  public void setClientForProposal(Integer id, AgentAddress client) {
    clientsPerProposal.put(id, client);
    proposalResponses.put(id, new ArrayList<>());
  }

  /**
   * Gets the client address associated to the provided proposal ID.
   *
   * @param id the proposal ID
   * @return the client address or {@code null} if this agent doesn't know the given ID.
   */
  public AgentAddress getClient(Integer id) {
    return clientsPerProposal.get(id);
  }
}
