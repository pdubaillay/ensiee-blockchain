package max.teaching.hyperledgerfabric.env;

import java.util.HashMap;
import java.util.Map;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.model.network.p2p.env.P2PContext;

/**
 * A customized {@link P2PContext} which stores additional information about (not) validated
 * transactions.
 *
 * @author Pierre Dubaillay
 */
public class ClientContext extends P2PContext {

  /** A mapping proposal ID to received validations count. */
  private final Map<Integer, Integer> validTxs;

  /** A mapping proposal ID to received invalid messages count. */
  private final Map<Integer, Integer> invalidTxs;

  /**
   * Creates a new {@link ClientContext} instance.
   *
   * @param owner the agent that owns the context
   * @param simulatedEnvironment the associated environment
   */
  public ClientContext(SimulatedAgent owner, SimulatedEnvironment simulatedEnvironment) {
    super(owner, simulatedEnvironment);
    validTxs = new HashMap<>();
    invalidTxs = new HashMap<>();
  }

  /**
   * Adds one to the count of valid messages for the provided proposal ID.
   *
   * @param proposalId ID of the proposal
   * @return the current count augmented by one, always at least 1
   */
  public Integer countOneValidForProposal(Integer proposalId) {
    return validTxs.compute(proposalId, (k, v) -> (v == null) ? 1 : v + 1);
  }

  /**
   * Adds one to the count of invalid messages for the provided proposal ID.
   *
   * @param proposalId ID of the proposal
   * @return the current count augmented by one, always at least 1
   */
  public Integer countOneInvalidForProposal(Integer proposalId) {
    return invalidTxs.compute(proposalId, (k, v) -> (v == null) ? 1 : v + 1);
  }

  /**
   * Clear the count of valid messages for the given proposal.
   *
   * @param proposalId  ID of the proposal
   */
  public void clearValidCountForProposal(Integer proposalId) {
    validTxs.remove(proposalId);
  }

  /**
   * Clear the count of invalid messages for the given proposal.
   *
   * @param proposalId  ID of the proposal
   */
  public void clearInvalidCountForProposal(Integer proposalId) {
    invalidTxs.remove(proposalId);
  }

}
