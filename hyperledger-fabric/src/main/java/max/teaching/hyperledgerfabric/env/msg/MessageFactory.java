package max.teaching.hyperledgerfabric.env.msg;

import java.math.BigDecimal;
import java.util.List;
import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Transaction;
import max.datatype.ledger.blockchain.Block;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalPayload;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalResponsePayload;

/**
 * The unique message factory which can create each message type.
 *
 * @author Pierre Dubaillay
 */
public final class MessageFactory {

  // The five different types of message.
  public static final String TX = "tx";
  public static final String TX_PROPOSAL = "tx_proposal";
  public static final String TX_PROPOSAL_RESPONSE = "tx_proposal_response";
  public static final String BLOCK = "block";
  public static final String NOTIFICATION = "notification";

  /**
   * Creates a new {@link #TX_PROPOSAL} message.
   *
   * @param sender   who will send the message
   * @param receiver who will receive the message
   * @param payload  the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null}
   */
  public static Message<AgentAddress, TxProposalPayload> createTxProposalMessage(
      AgentAddress sender, AgentAddress receiver, TxProposalPayload payload) {
    return createMessageOfType(TX_PROPOSAL, sender, List.of(receiver), payload);
  }

  /**
   * Creates a new {@link #TX_PROPOSAL} message.
   *
   * @param sender    who will send the message
   * @param receivers who will receive the message
   * @param payload   the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null} or
   *                                  receivers is an empty list
   */
  public static Message<AgentAddress, TxProposalPayload> createTxProposalMessage(
      AgentAddress sender, List<AgentAddress> receivers, TxProposalPayload payload) {
    return createMessageOfType(TX_PROPOSAL, sender, receivers, payload);
  }

  /**
   * Creates a new {@link #TX_PROPOSAL_RESPONSE} message.
   *
   * @param sender   who will send the message
   * @param receiver who will receive the message
   * @param payload  the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null}
   */
  public static Message<AgentAddress, TxProposalResponsePayload> createTxProposalResponseMessage(
      AgentAddress sender, AgentAddress receiver, TxProposalResponsePayload payload) {
    return createMessageOfType(TX_PROPOSAL_RESPONSE, sender, List.of(receiver), payload);
  }

  /**
   * Creates a new {@link #NOTIFICATION} message.
   *
   * @param sender   who will send the message
   * @param receiver who will receive the message
   * @param payload  the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null}
   */
  public static Message<AgentAddress, NotificationPayload> createNotificationMessage(
      AgentAddress sender, AgentAddress receiver, NotificationPayload payload) {
    return createMessageOfType(NOTIFICATION, sender, List.of(receiver), payload);
  }

  /**
   * Creates a new {@link #TX} message.
   *
   * @param sender    who will send the message
   * @param receivers who will receive the message
   * @param payload   the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null} or
   *                                  receivers is empty
   */
  public static Message<AgentAddress, Transaction> createTxMessage(AgentAddress sender,
      List<AgentAddress> receivers, Transaction payload) {
    return createMessageOfType(TX, sender, receivers, payload);
  }

  /**
   * Creates a new {@link #BLOCK} message.
   *
   * @param sender    who will send the message
   * @param receivers who will receive the message
   * @param payload   the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null} or
   *                                  receivers is empty
   */
  public static Message<AgentAddress, Block<Transaction, BigDecimal>> createBlockMessage(
      AgentAddress sender, List<AgentAddress> receivers, Block<Transaction, BigDecimal> payload) {
    return createMessageOfType(BLOCK, sender, receivers, payload);
  }

  /**
   * Creates a new message.
   *
   * @param <T>       the payload's type
   * @param type      the message's type
   * @param sender    who will send the message
   * @param receivers who will receive the message
   * @param payload   the message's content
   * @return the message instance, never {@code null}
   * @throws IllegalArgumentException at least one of the provided arguments is {@code null} or
   *                                  receivers is empty.
   */
  private static <T> Message<AgentAddress, T> createMessageOfType(String type, AgentAddress sender,
      List<AgentAddress> receivers, T payload) {
    if (type == null || sender == null || receivers == null || receivers.isEmpty()
        || payload == null) {
      throw new IllegalArgumentException();
    }

    final var msg = new Message<>(sender, receivers, payload);
    msg.setType(type);
    return msg;
  }

  private MessageFactory() {
    // Empty
  }

}
