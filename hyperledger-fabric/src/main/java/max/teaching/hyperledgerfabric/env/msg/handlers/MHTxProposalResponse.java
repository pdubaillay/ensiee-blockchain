package max.teaching.hyperledgerfabric.env.msg.handlers;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.datatype.ledger.Payload;
import max.model.ledger.blockchain.role.RBlockCreator;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.model.network.p2p.env.message.MessageHandler;
import max.teaching.hyperledgerfabric.TxPayload;
import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.env.TxFactory;
import max.teaching.hyperledgerfabric.env.msg.MessageFactory;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload;
import max.teaching.hyperledgerfabric.env.msg.payloads.NotificationPayload.Status;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalResponsePayload;

/**
 * A dedicated handler that handles {@link MessageFactory#TX_PROPOSAL_RESPONSE} messages.
 * <p>
 * This handler will only be call if the endorser is the client's relay. Proposal responses are
 * collected and, if the number of proposal responses reaches the number of endorsers in the system,
 * then a transaction is created and sent to the ordering service. The client is also notified of
 * its proposal status: either {@link Status#ENDORSED} if all endorsers successfully endorsed the
 * proposal, or {@link Status#NOT_ENDORSED} if at least one endorser responded with an error.
 *
 * @author Pierre Dubaillay
 */
public class MHTxProposalResponse implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    final var payload = (TxProposalResponsePayload) message.getPayload();
    final var ctx = (EndorserContext) p2PContext;

    // Collect the response
    ctx.addProposalResponse(payload);

    final var endorsers = ctx.getOwner().getAgentsWithRole(ctx.getEnvironmentName(),
        RTransactionEndorser.class);
    final var proposals = ctx.getProposalResponsesFor(payload.getProposalId());

    // Did we collect enough responses ?
    if (proposals.size() == endorsers.size() + 1) {
      final var hasError = proposals.stream().anyMatch(p -> p.getError() != null);

      NotificationPayload nPayload;
      if (!hasError) {  // Everyone agrees , no errors
        nPayload = new NotificationPayload(Status.ENDORSED, payload.getProposalId(),
            proposals.get(0).getResult().getReturnValue());
      } else { // At least one error
        final var error = proposals.stream().filter(p -> p.getError() != null).findFirst();
        nPayload = new NotificationPayload(Status.NOT_ENDORSED, payload.getProposalId(),
            error.get().getError());
      }

      // Notify client
      final var client = ctx.getClient(payload.getProposalId());
      final var me = ctx.getOwner()
          .getAgentAddressIn(ctx.getEnvironmentName(), RTransactionEndorser.class);
      ((P2PEnvironment) ctx.getEnvironment()).sendMessage(MessageFactory.createNotificationMessage(
          me, client, nPayload
      ));

      // Clear the endorser's context since we can decide
      ctx.forgotProposal(payload.getProposalId());

      // Send the transaction to the ordering service if the proposal is endorsed
      if (nPayload.getStatus() == Status.ENDORSED) {
        final var orderers = ctx.getOwner()
            .getAgentsWithRole(ctx.getEnvironmentName(), RBlockCreator.class);
        ((P2PEnvironment) ctx.getEnvironment()).sendMessage(MessageFactory.createTxMessage(
            me,
            orderers,
            new TxFactory().createTransaction(null, null, 0,
                new Payload(new TxPayload(client, payload.getProposalId(), proposals)))
        ));
      }
    }
  }
}
