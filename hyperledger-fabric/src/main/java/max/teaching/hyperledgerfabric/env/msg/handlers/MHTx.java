package max.teaching.hyperledgerfabric.env.msg.handlers;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.message.MessageHandler;

/**
 * A dedicated handler that handles {@link max.teaching.hyperledgerfabric.env.msg.MessageFactory#TX}
 * messages.
 * <p>
 * Executed by the ordering service, each transaction contained in the message's payload is added to
 * the agent's {@link max.datatype.ledger.blockchain.MemoryPool} to be later selected and added in a
 * block.
 *
 * @author Pierre Dubaillay
 */
public class MHTx implements MessageHandler {

  @Override
  public void handle(P2PContext p2PContext, Message<AgentAddress, ?> message) {
    /* TO BE IMPLEMENTED */
  }
}
