package max.teaching.hyperledgerfabric;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

/**
 * A {@link ResultSet} contains two sets:
 * <ul>
 *   <li>The read set, which itself contains read entries.</li>
 *   <li>The write set, which itself contains write entries.</li>
 * </ul>
 * <p>
 * The {@link ResultSet} is created during the proposal execution phase and later used by endorsers
 * in the validation one. It provides information about the world state at execution time (read
 * entries) and what changes (deletion, creation or modification of assets, aka write entries) the
 * transaction will produce.
 *
 * @author Pierre Dubaillay
 */
public class ResultSet {

  /**
   * A {@link ReadEntry} stores the version of a read asset during the execution phase.
   *
   * @author Pierre Dubaillay
   */
  public static class ReadEntry {

    /**
     * The ID of the asset.
     */
    private final String key;

    /**
     * The version of the asset.
     */
    private final int version;

    /**
     * Creates a new {@link ReadEntry} instance.
     *
     * @param key     asset's ID
     * @param version asset's version
     * @throws IllegalArgumentException key is {@code null} or version is a negative number
     */
    public ReadEntry(String key, int version) {
      if (key == null || version < 0) {
        throw new IllegalArgumentException();
      }
      this.key = key;
      this.version = version;
    }

    /**
     * Gets the asset's version.
     *
     * @return a positive integer (including 0)
     */
    public int getVersion() {
      return version;
    }

    /**
     * Gets the asset's ID.
     *
     * @return never {@code null}
     */
    public String getKey() {
      return key;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      ReadEntry readEntry = (ReadEntry) o;
      return key.equals(readEntry.key) && version == readEntry.version;
    }

    @Override
    public int hashCode() {
      return Objects.hash(key, version);
    }

    @Override
    public String toString() {
      return "R(" + key + ", " + version + ")";
    }

  }

  /**
   * A {@link WriteEntry} stores modifications about one asset.
   *
   * @author Pierre Dubaillay
   */
  public static class WriteEntry {

    /** Asset's ID. */
    private final String key;

    /** The new asset's state. */
    private final Asset value;

    /** Tells if this is an asset deletion. */
    private final boolean isDelete;

    /**
     * Creates a new {@link WriteEntry} storing the creation or modification of an asset.
     *
     * @param value the new asset's state
     * @throws IllegalArgumentException value is {@code null}
     */
    public WriteEntry(Asset value) {
      if (value == null) {
        throw new IllegalArgumentException();
      }
      this.key = value.getId();
      this.value = value;
      isDelete = false;
    }

    /**
     * Creates a new {@link WriteEntry} storing the deletion of an asset.
     *
     * @param key asset's id
     * @throws IllegalArgumentException key is {@code null}
     */
    public WriteEntry(String key) {
      if (key == null) {
        throw new IllegalArgumentException();
      }
      this.key = key;
      isDelete = true;
      value = null;
    }

    /**
     * Gets the asset's ID.
     *
     * @return never {@code null}
     */
    public String getKey() {
      return key;
    }

    /**
     * Gets the asset's new state.
     *
     * @return either the asset or {@code null} if the entry is about a deletion
     * @param <T> asset's type
     * @throws ClassCastException the asset can't be cast to the provided type
     */
    public <T extends Asset> T getValue() {
      return (T) value;
    }

    /**
     * Tells if this entry is about the deletion of an asset.
     *
     * @return {@code true} if it is a deletion, {@code false} otherwise
     */
    public boolean isDelete() {
      return isDelete;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      WriteEntry that = (WriteEntry) o;
      return isDelete == that.isDelete && key.equals(that.key) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
      return Objects.hash(key, value, isDelete);
    }

    @Override
    public String toString() {
      return "W(" + key + ", " + value + ")";
    }
  }

  /** The collection of read entries. */
  private final Collection<ReadEntry> readSet;

  /** The collection of write entries. */
  private final Collection<WriteEntry> writeSet;

  /**
   * Creates a new {@link ResultSet} instance, initially empty.
   */
  public ResultSet() {
    readSet = new HashSet<>();
    writeSet = new HashSet<>();
  }

  /**
   * Creates a new {@link ResultSet} instance and supply both read and write sets.
   *
   * @param readSet a collection of read entries
   * @param writeSet a collection of write entries
   * @throws IllegalArgumentException either readSet or writeSet is {@code null}
   */
  public ResultSet(Collection<ReadEntry> readSet, Collection<WriteEntry> writeSet) {
    this();
    if (readSet == null || writeSet == null) {
      throw new IllegalArgumentException();
    }
    this.readSet.addAll(readSet);
    this.writeSet.addAll(writeSet);
  }

  /**
   * Gets the collection of read entries.
   *
   * @return a view of the read set, never {@code null}
   */
  public Collection<ReadEntry> getReadSet() {
    return Collections.unmodifiableCollection(readSet);
  }

  /**
   * Gets the collection of write entries.
   *
   * @return a view of the write set, never {@code null}
   */
  public Collection<WriteEntry> getWriteSet() {
    return Collections.unmodifiableCollection(writeSet);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResultSet resultSet = (ResultSet) o;
    return readSet.equals(resultSet.readSet) && writeSet.equals(resultSet.writeSet);
  }

  @Override
  public int hashCode() {
    return Objects.hash(readSet, writeSet);
  }
}
