package max.teaching.hyperledgerfabric;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import madkit.kernel.AgentAddress;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalResponsePayload;

/**
 * The content of a transaction in our model.
 * <p>
 * It stores the given information:
 * <ul>
 *   <li>The client's address</li>
 *   <li>The proposal ID</li>
 *   <li>The list of transaction proposal responses attesting the transaction is endorsed.</li>
 * </ul>
 */
public class TxPayload {

  /**
   * The client's address.
   */
  private final AgentAddress client;

  /**
   * The proposal ID.
   */
  private final int proposalId;

  /**
   * The list of proposal responses.
   */
  private final List<TxProposalResponsePayload> responses;

  /**
   * Creates a new {@link TxPayload} and supply the necessary information.
   *
   * @param client    the client's address
   * @param proposal  the proposal ID
   * @param responses the list of responses
   * @throws IllegalArgumentException client or responses is {@code null}, proposal is a negative
   *                                  number or responses is empty
   */
  public TxPayload(AgentAddress client, int proposal,
      List<TxProposalResponsePayload> responses) {
    if (client == null || proposal < 0 || responses == null || responses.isEmpty()) {
      throw new IllegalArgumentException();
    }
    this.client = client;
    this.proposalId = proposal;
    this.responses = responses;
  }

  /**
   * Gets a view of the list of responses.
   *
   * @return never {@code null}, at least one element in the view
   */
  public List<TxProposalResponsePayload> getResponses() {
    return Collections.unmodifiableList(responses);
  }

  /**
   * Gets the proposal ID.
   *
   * @return a positive integer (0 included)
   */
  public int getProposalId() {
    return proposalId;
  }

  /**
   * Gets the client address.
   *
   * @return never {@code null}
   */
  public AgentAddress getClient() {
    return client;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TxPayload txPayload = (TxPayload) o;
    return client.equals(txPayload.client) && proposalId == txPayload.proposalId
        && responses.equals(
        txPayload.responses);
  }

  @Override
  public int hashCode() {
    return Objects.hash(client, proposalId, responses);
  }
}
