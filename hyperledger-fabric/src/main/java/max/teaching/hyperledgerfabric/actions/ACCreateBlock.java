package max.teaching.hyperledgerfabric.actions;

import max.core.action.Action;
import max.model.ledger.blockchain.role.RBlockCreator;
import max.teaching.hyperledgerfabric.agents.OrdererAgent;

/**
 * Creates a new block, adds it in the owner's blockchain and propagates it to transaction
 * executors.
 *
 * @author Pierre Dubaillay
 * @max.role {@link RBlockCreator}
 */
public class ACCreateBlock extends Action<OrdererAgent> {

  /**
   * Create a new {@link ACCreateBlock} instance running in the given environment, requiring the
   * given role and having the provided owner.
   *
   * @param environment Where the action will run
   * @param owner       Action's owner
   */
  public ACCreateBlock(String environment, OrdererAgent owner) {
    super(environment, RBlockCreator.class, owner);
  }

  @Override
  public void execute() {
    /* TO BE IMPLEMENTED */
  }

  @Override
  public <T extends Action<OrdererAgent>> T copy() {
    return (T) new ACCreateBlock(getEnvironment(), getOwner());
  }
}
