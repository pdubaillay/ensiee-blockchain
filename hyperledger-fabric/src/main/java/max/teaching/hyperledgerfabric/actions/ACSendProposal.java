package max.teaching.hyperledgerfabric.actions;

import max.core.action.Action;
import max.model.ledger.blockchain.role.RBlockchainClient;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.network.p2p.env.P2PContext;
import max.model.network.p2p.env.P2PEnvironment;
import max.teaching.hyperledgerfabric.agents.ClientAgent;
import max.teaching.hyperledgerfabric.env.msg.MessageFactory;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalPayload;

/**
 * Sends the given TX Proposal to one endorser.
 *
 * @author Pierre Dubaillay
 * @max.role {@link RBlockchainClient}
 */
public class ACSendProposal extends Action<ClientAgent> {

  /**
   * The payload of the proposal to send.
   */
  private final TxProposalPayload payload;

  /**
   * Creates a new {@link ACSendProposal} instance which will send the provided
   * {@link TxProposalPayload} to one endorser (playing the {@link RTransactionEndorser} role).
   *
   * @param env where the action will run
   * @param owner who will run it
   * @param payload the proposal payload
   */
  public ACSendProposal(String env, ClientAgent owner, TxProposalPayload payload) {
    super(env, RBlockchainClient.class, owner);
    this.payload = payload;
  }

  @Override
  public void execute() {
    final var ctx = (P2PContext) getOwner().getContext(getEnvironment());
    final var peer = getOwner().getAgentsWithRole(getEnvironment(), RTransactionEndorser.class);
    final var msg = MessageFactory.createTxProposalMessage(
        ctx.getMyAddress(RBlockchainClient.class.getName()), peer.get(0), payload);
    ((P2PEnvironment) ctx.getEnvironment()).sendMessage(msg);
  }

  @Override
  public <T extends Action<ClientAgent>> T copy() {
    return (T) new ACSendProposal(getEnvironment(), getOwner(), payload);
  }
}
