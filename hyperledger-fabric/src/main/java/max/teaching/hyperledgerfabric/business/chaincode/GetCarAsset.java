package max.teaching.hyperledgerfabric.business.chaincode;

import java.util.List;
import max.teaching.hyperledgerfabric.Chaincode;
import max.teaching.hyperledgerfabric.exceptions.ExecutionException;
import max.teaching.hyperledgerfabric.Result;
import max.teaching.hyperledgerfabric.ResultSet;
import max.teaching.hyperledgerfabric.ResultSet.ReadEntry;
import max.teaching.hyperledgerfabric.env.EndorserContext;

/**
 * A chaincode to retrieve a car asset's state given its ID.
 * <p>
 * The execution of the chaincode may fail if the provided ID is not stored in the endorser's
 * database.
 *
 * @author Pierre Dubaillay
 */
public class GetCarAsset implements Chaincode {

  /**
   * The ID of the asset to get.
   */
  private final String id;

  /**
   * Creates a new instance of the {@link GetCarAsset} chaincode.
   *
   * @param id the asset's ID
   */
  public GetCarAsset(String id) {
    this.id = id;
  }

  @Override
  public Result execute(EndorserContext ctx) throws ExecutionException {
    final var asset = ctx.getAsset(id);
    if (asset == null) {
      throw new ExecutionException("Unknown asset with id=" + id);
    }

    final var version = asset.getVersion();
    try {
      final var cpy = asset.copy();
      return new Result(
          new ResultSet(List.of(new ReadEntry(id, version)), List.of()),
          cpy
      );
    } catch (ReflectiveOperationException e) {
      throw new ExecutionException(e);
    }
  }
}
