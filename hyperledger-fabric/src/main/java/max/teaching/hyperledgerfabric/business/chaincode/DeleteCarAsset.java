package max.teaching.hyperledgerfabric.business.chaincode;

import java.util.List;
import max.teaching.hyperledgerfabric.Chaincode;
import max.teaching.hyperledgerfabric.Result;
import max.teaching.hyperledgerfabric.ResultSet;
import max.teaching.hyperledgerfabric.ResultSet.WriteEntry;
import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.exceptions.ExecutionException;

/**
 * Deletes a car asset given its ID.
 *
 * @author Pierre Dubaillay
 */
public class DeleteCarAsset implements Chaincode {

  /** The car id. */
  private final String id;

  /**
   * Creates a new {@link DeleteCarAsset} instance and supply the id.
   *
   * @param id the car's id
   */
  public DeleteCarAsset(String id) {
    this.id = id;
  }

  @Override
  public Result execute(EndorserContext ctx) throws ExecutionException {
    if (ctx.getAsset(id) == null) {
      throw new ExecutionException("Unknown asset id=" + id);
    }
    return new Result(
        new ResultSet(List.of(), List.of(new WriteEntry(id))),
        null
    );
  }
}
