package max.teaching.hyperledgerfabric.business.chaincode;

import java.util.List;
import max.teaching.hyperledgerfabric.Chaincode;
import max.teaching.hyperledgerfabric.Result;
import max.teaching.hyperledgerfabric.ResultSet;
import max.teaching.hyperledgerfabric.ResultSet.WriteEntry;
import max.teaching.hyperledgerfabric.ResultSet.ReadEntry;
import max.teaching.hyperledgerfabric.business.CarAsset;
import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.exceptions.ExecutionException;

/**
 * Changes the owner of an existing car asset.
 *
 * @author Pierre Dubaillay
 */
public class ChangeCarOwner implements Chaincode {

  /** Car's id. */
  private final String id;

  /** The new owner. */
  private final String newOwner;

  /**
   * Creates a new {@link ChangeCarOwner} instance.
   *
   * @param id car's id
   * @param newOwner the new owner
   */
  public ChangeCarOwner(String id, String newOwner) {
    this.id = id;
    this.newOwner = newOwner;
  }

  @Override
  public Result execute(EndorserContext ctx) throws ExecutionException {
    final var asset = (CarAsset) ctx.getAsset(id);
    if (asset == null) {
      throw new ExecutionException("Unknown asset id=" + id);
    }

    try {
      final CarAsset cpy = asset.copy();
      cpy.owner = newOwner;

      return new Result(
          new ResultSet(List.of(new ReadEntry(id, asset.getVersion())),
              List.of(new WriteEntry(cpy))),
          null
      );

    } catch (ReflectiveOperationException e) {
      throw new ExecutionException(e);
    }
  }
}
