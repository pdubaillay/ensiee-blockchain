package max.teaching.hyperledgerfabric.business.chaincode;

import java.util.List;
import max.teaching.hyperledgerfabric.Chaincode;
import max.teaching.hyperledgerfabric.exceptions.ExecutionException;
import max.teaching.hyperledgerfabric.Result;
import max.teaching.hyperledgerfabric.ResultSet;
import max.teaching.hyperledgerfabric.ResultSet.WriteEntry;
import max.teaching.hyperledgerfabric.business.CarAsset;
import max.teaching.hyperledgerfabric.env.EndorserContext;

/**
 * This chaincode can create a new {@link CarAsset}.
 * <p>
 * The chaincode execution might fail if the provided ID is already associated to another asset.
 *
 * @author Pierre Dubaillay
 */
public class CreateCarAsset implements Chaincode {

  /**
   * The future car id.
   */
  private final String id;

  /**
   * The name of the car manufacturer.
   */
  private final String manufacturer;

  /**
   * The car's model.
   */
  private final String model;

  /**
   * The color of the car.
   */
  private final String color;

  /**
   * How many seats the car have.
   */
  private final int seats;

  /**
   * Creates a new instance of the chaincode.
   *
   * @param id           car's id
   * @param manufacturer car's manufacturer
   * @param model        car's model
   * @param color        car's color
   * @param seats        how many seats the car have
   */
  public CreateCarAsset(String id, String manufacturer, String model, String color, int seats) {
    this.id = id;
    this.manufacturer = manufacturer;
    this.model = model;
    this.color = color;
    this.seats = seats;
  }

  @Override
  public Result execute(EndorserContext ctx) throws ExecutionException {
    if (ctx.getAsset(id) != null) {
      throw new ExecutionException("Duplicated ID");
    }

    final var asset = new CarAsset(id);
    asset.color = color;
    asset.manufacturer = manufacturer;
    asset.model = model;
    asset.seats = seats;

    return new Result(
        new ResultSet(List.of(), List.of(new WriteEntry(asset))),
        null
    );
  }

}
