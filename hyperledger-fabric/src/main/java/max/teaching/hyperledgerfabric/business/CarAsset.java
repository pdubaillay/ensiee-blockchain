package max.teaching.hyperledgerfabric.business;

import max.teaching.hyperledgerfabric.Asset;

/**
 * A specialized asset to represent a specific car.
 *
 * @author Pierre Dubaillay
 */
public class CarAsset extends Asset {

  /**
   * The car's manufacturer.
   */
  public String manufacturer;

  /**
   * The car's model.
   */
  public String model;

  /**
   * The car's color.
   */
  public String color;

  /**
   * How many seats the car have.
   */
  public int seats;

  /**
   * The car's owner.
   */
  public String owner;

  /**
   * Creates a new {@link CarAsset} instance, with the provided ID.
   * <p>
   * Other fields are initialized to their default value.
   *
   * @param id Asset's id
   */
  public CarAsset(String id) {
    super(id);
  }

}
