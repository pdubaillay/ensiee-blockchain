package max.teaching.hyperledgerfabric.exceptions;

/**
 * An exception telling that the transaction validation failed.
 *
 * @author Pierre Dubaillay
 */
public class ValidationException extends Exception {

  /**
   * Creates a new {@link ValidationException} telling that a read failed because of some versions
   * mismatch.
   *
   * @param expected expected version
   * @param found    found version
   */
  public ValidationException(int expected, int found) {
    super("Version mismatch: expected " + expected + " but found " + found);
  }

  /**
   * Creates a new {@link ValidationException} instance telling that something failed during
   * validation.
   *
   * @param msg the reason
   */
  public ValidationException(String msg) {
    super(msg);
  }

  /**
   * Creates a new {@link ValidationException} instance telling the validation phase failed because
   * of some provided exception.
   *
   * @param cause why validation failed
   */
  public ValidationException(Throwable cause) {
    super(cause);
  }

}
