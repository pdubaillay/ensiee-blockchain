package max.teaching.hyperledgerfabric.exceptions;

/**
 * An exception telling that the chaincode execution failed for some reason.
 *
 * @author Pierre Dubaillay
 */
public class ExecutionException extends Exception {

  /**
   * Creates a new {@link ExecutionException} instance, telling the chaincode execution failed
   * because of another error.
   *
   * @param e the error
   */
  public ExecutionException(Throwable e) {
    super(e);
  }

  /**
   * Creates a new {@link ExecutionException} instance, telling the chaincode execution failed
   * because of some custom reason.
   *
   * @param msg some details about why the exception is thrown
   */
  public ExecutionException(String msg) {
    super(msg);
  }
}
