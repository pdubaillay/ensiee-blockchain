package max.teaching.hyperledgerfabric;

import java.util.Objects;

/**
 * Base class for all business assets.
 *
 * @author Pierre Dubaillay
 */
public abstract class Asset {

  /**
   * The asset's id, which must be unique.
   */
  private final String id;

  /**
   * The asset version.
   */
  private int version;

  /**
   * Creates a new {@link Asset} instance with the provided ID.
   *
   * Version is set to 0.
   *
   * @param id the asset's ID
   * @throws IllegalArgumentException id is {@code null}
   */
  protected Asset(String id) {
    if (id == null) {
      throw new IllegalArgumentException();
    }
    this.id = id;
    this.version = 0;
  }

  /**
   * Gets this asset's ID.
   *
   * @return never {@code null}
   */
  public String getId() {
    return id;
  }

  /**
   * Increases by 1 the asset's version.
   */
  public void increaseVersion() {
    version += 1;
  }

  /**
   * Gets the current asset's version.
   *
   * @return >= 0
   */
  public int getVersion() {
    return version;
  }

  /**
   * Copies the current asset.
   * <p>
   * This generic implementation copies each field using reflexive operations, but feel free to
   * override it in your own asset.
   *
   * @param <T> Type of the asset
   * @return a copy of the current asset
   * @throws ReflectiveOperationException the copy failed.
   */
  public <T extends Asset> T copy() throws ReflectiveOperationException {
    final var constructor = getClass().getConstructor(String.class);
    final var cpy = constructor.newInstance(getId());
    for (final var field : getClass().getFields()) {
      field.set(cpy, field.get(this));
    }
    return (T) cpy;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Asset asset = (Asset) o;
    return id.equals(asset.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    final var builder = new StringBuilder(getClass().getName() + "(");
    for (final var field : getClass().getFields()) {
      builder.append(field.getName());
      builder.append("=");
      try {
        builder.append(field.get(this));
      } catch (IllegalAccessException e) {
        // ignore
      }
      builder.append(" ");
    }
    builder.replace(builder.length() - 1, builder.length() - 1, ")");
    return builder.toString();
  }
}
