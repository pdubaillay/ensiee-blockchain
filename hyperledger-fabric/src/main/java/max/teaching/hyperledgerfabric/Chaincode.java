package max.teaching.hyperledgerfabric;

import max.teaching.hyperledgerfabric.env.EndorserContext;
import max.teaching.hyperledgerfabric.exceptions.ExecutionException;

/**
 * Common interface for all chaincodes.
 * <p>
 * It declares only one method called {@link #execute(EndorserContext)} which takes the endorser's
 * context (for access to the world state) and return a {@link Result}, which contains the
 * read/write set and an eventual result.
 */
public interface Chaincode {

  /**
   * Executes the chaincode.
   * <p>
   * The {@link Result} instance will contain the read/write set and an eventual
   * returned value. If the execution fails, an {@link ExecutionException} is thrown.
   *
   * @param ctx the endorser's context
   * @return a {@link Result} instance, never {@code null}
   * @throws ExecutionException the execution failed for some specified reason
   */
  Result execute(EndorserContext ctx) throws ExecutionException;
}
