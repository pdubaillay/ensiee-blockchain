package max.teaching.hyperledgerfabric;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import max.core.EnvironmentEvent;
import max.core.MAXParameters;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.action.ReactiveAction;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.core.test.TestMain;
import max.model.ledger.blockchain.role.RBlockCreator;
import max.model.ledger.blockchain.role.RBlockchainClient;
import max.model.ledger.blockchain.role.RTransactionEndorser;
import max.model.ledger.blockchain.role.RTransactionExecutor;
import max.model.network.p2p.action.ACHandleMessages;
import max.model.network.p2p.env.message.MessageEvent;
import max.teaching.hyperledgerfabric.actions.ACCreateBlock;
import max.teaching.hyperledgerfabric.actions.ACSendProposal;
import max.teaching.hyperledgerfabric.agents.ClientAgent;
import max.teaching.hyperledgerfabric.agents.EndorserAgent;
import max.teaching.hyperledgerfabric.agents.OrdererAgent;
import max.teaching.hyperledgerfabric.env.Channel;
import max.teaching.hyperledgerfabric.env.msg.MessageFactory;
import max.teaching.hyperledgerfabric.env.msg.handlers.MHBlock;
import max.teaching.hyperledgerfabric.env.msg.handlers.MHNotification;
import max.teaching.hyperledgerfabric.env.msg.handlers.MHTx;
import max.teaching.hyperledgerfabric.env.msg.handlers.MHTxProposal;
import max.teaching.hyperledgerfabric.env.msg.handlers.MHTxProposalResponse;
import max.teaching.hyperledgerfabric.env.msg.payloads.TxProposalPayload;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * A simple scenario to demonstrate how a Hyperledger Fabric blockchain works.
 *
 * @author Pierre Dubaillay
 */
class SimpleScenarioTest {

  @BeforeAll
  static void beforeAll(@TempDir Path tempDir) {
    MAXParameters.setParameter("MAX_CORE_SIMULATION_STEP", "1");
    MAXParameters.setParameter("MAX_MODEL_NETWORK_RELIABILITY", "1");
    MAXParameters.setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
  }

  @Test
  void chaincodeInvocationTest(TestInfo testInfo) throws Throwable {
    final var tester = new ExperimenterAgent() {

      @Override
      protected List<MAXAgent> setupScenario() {
        final var agents = new ArrayList<MAXAgent>();

        // Environment
        final var env = new Channel();
        agents.add(env);

        // Client
        final var client = new ClientAgent(new Plan<ClientAgent>() {
          @Override
          public List<ActionActivator<ClientAgent>> getInitialPlan() {
            return List.of(
                new ACTakeRole<ClientAgent>(env.getName(), RBlockchainClient.class, null).oneTime(
                    1),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.CreateCarAsset",
                        List.of("0", "BMW", "i8", "red", 4))).oneTime(2),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.GetCarAsset",
                        List.of("0"))).oneTime(4),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.ChangeCarOwner",
                        List.of("0", "ENSIEE"))).oneTime(6),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.GetCarAsset",
                        List.of("0"))).oneTime(8),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.DeleteCarAsset",
                        List.of("0"))).oneTime(10),
                new ACSendProposal(env.getName(), null,
                    new TxProposalPayload(
                        "max.teaching.hyperledgerfabric.business.chaincode.GetCarAsset",
                        List.of("0"))).oneTime(12));
          }

          @Override
          public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<ClientAgent>>> getEventBoundActions() {
            final var handler = new ACHandleMessages<ClientAgent>(env.getName(), null);
            handler.registerMessageType(MessageFactory.NOTIFICATION, new MHNotification());

            return Map.of(MessageEvent.class, List.of(handler));
          }
        });
        agents.add(client);

        // Endorsers
        for (int i = 0; i < 1; i++) {
          agents.add(new EndorserAgent(createEndorserPlan(env.getName())));
        }

        // Ordering service
        agents.add(new OrdererAgent(createOrdererPlan(env.getName())));

        return agents;
      }
    };

    TestMain.launchTester(tester, 20, testInfo);
  }

  /**
   * Creates a plan for an Endorser agent.
   * <p>
   * The agent will take two roles at tick 1 (RTransactionEndorser and RTransactionExecutor) and can
   * handle three messages types : TX_PROPOSAL, TX_PROPOSAL_RESPONSE and BLOCK.
   *
   * @param envName where the agent is deployed
   * @return the plan
   */
  private Plan<EndorserAgent> createEndorserPlan(String envName) {
    return new Plan<>() {
      @Override
      public List<ActionActivator<EndorserAgent>> getInitialPlan() {
        return List.of(
            new ACTakeRole<EndorserAgent>(envName,
                RTransactionEndorser.class, null).oneTime(1),
            new ACTakeRole<EndorserAgent>(envName, RTransactionExecutor.class,
                null).oneTime(1));
      }

      @Override
      public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<EndorserAgent>>> getEventBoundActions() {
        final var handler = new ACHandleMessages<EndorserAgent>(envName,
            null);
        handler.registerMessageType(MessageFactory.TX_PROPOSAL, new MHTxProposal());
        handler.registerMessageType(MessageFactory.TX_PROPOSAL_RESPONSE,
            new MHTxProposalResponse());
        handler.registerMessageType(MessageFactory.BLOCK, new MHBlock());
        return Map.of(MessageEvent.class, List.of(handler));
      }
    };
  }

  /**
   * Creates a plan for our ordering service.
   * <p>
   * The Orderer will take the RBlockCreator role at tick 1 and create blocks every 2 ticks starting
   * at tick 3. The agent can handle one type of message : TX.
   *
   * @param envName where the agent is deployed
   * @return the plan
   */
  private Plan<OrdererAgent> createOrdererPlan(String envName) {
    /* TO BE IMPLEMENTED */
    return null;
  }

}
